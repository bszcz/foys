// Copyright (c) 2012-2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

Rectangle = function( label, r, g, b, w, h, x, y, dx, dy ) {
	this.label = label;
	this.r = r;
	this.g = g;
	this.b = b;
	this.w = w;
	this.h = h;
	this.x = x;
	this.y = y;
	this.dx = dx;
	this.dy = dy;
}


Rectangle.prototype.Draw = function() {
	//ge.ctx.globalAlpha = 0.8;
	//ge.ctx.shadowOffsetX = 5;
	//ge.ctx.shadowOffsetY = 5;
	//ge.ctx.shadowBlur = 5;
	//ge.ctx.shadowColor = "rgb(" + this.r +','+ this.g +','+ this.b +")"
	ge.ctx.fillStyle = "rgb(" + this.r +','+ this.g +','+ this.b +")";
	ge.ctx.fillRect(this.x, this.y, this.w, this.h);

	if ( this.label.length > 0 ) {
		ge.ctx.font = '28px sans-serif';
		ge.ctx.fillStyle = 'white';
		ge.ctx.fillText( this.label, this.x + 4 , this.y + this.h - 8 );
	}
}


Rectangle.prototype.Move = function( timeDelay ) {
	if ( timeDelay == undefined ) {
		timeDelay = 1;
	}

	this.x += ge.timeDelta * this.dx / timeDelay;
	this.y += ge.timeDelta * this.dy / timeDelay;
}


Rectangle.prototype.Intersects = function( other ) {
	var x1 = ( this.x < other.x ) ? this.x : other.x;
	var y1 = ( this.y < other.y ) ? this.y : other.y;
	var x2 = ( this.x+this.w > other.x+other.w ) ? this.x+this.w : other.x+other.w;
	var y2 = ( this.y+this.h > other.y+other.h ) ? this.y+this.h : other.y+other.h;
	var inter_w = x2 - x1;
	var inter_h = y2 - y1;

	if ( this.w+other.w > inter_w && this.h+other.h > inter_h ) {
		return true;
	} else {
		return false;
	}
}
