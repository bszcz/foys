// Copyright (c) 2012-2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

function AlertText( text, r, g, b, x, y ) {
	this.text = text;
	this.r = r;
	this.g = g;
	this.b = b;
	this.x = x;
	this.y = y;
}


AlertText.prototype.Draw = function() {
	ge.ctx.font = '24px sans-serif';
	ge.ctx.fillStyle = "rgb(" + this.r +','+ this.g +','+ this.b +")";
	ge.ctx.fillText( this.text, this.x, this.y - 8 );
}


function Alerts() {
	this.ALERT_DY = 0.1;
	this.texts = new Array();
}


Alerts.prototype.Update = function() {
	var t = this.texts.length;
	while ( t-- ) {
		this.texts[t].y -= ge.timeDelta * this.ALERT_DY;

		this.texts[t].r += 1;
		this.texts[t].g += 1;
		this.texts[t].b += 1;

		if ( this.texts[t].r > 255 ) this.texts[t].r = 255;
		if ( this.texts[t].g > 255 ) this.texts[t].g = 255;
		if ( this.texts[t].b > 255 ) this.texts[t].b = 255;

		if ( this.texts[t].r+this.texts[t].g+this.texts[t].b >= 3*255 ) {
			this.texts.splice( t, 1 );
		}
	}
	//console.log( "alerts.texts.length = " + this.texts.length );
}
