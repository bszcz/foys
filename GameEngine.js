// Copyright (c) 2012-2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

function GameEngine() {
	this.CANV_W = 900;
	this.CANV_H = 450;

	this.KEY_DOWN = 1;
	this.KEY_UP = 0;

	this.lastAnimationTimestamp = 0;
	this.timeDelta = 0;
	this.timeDeltaMax = 50;
	this.timeDelay = 1;
	this.timeDelayCounter = 0;

	this.ctx = null;
	this.canv = null;

	this.pauseOn = false;
	this.pauseBlock = false;
	this.gameOver = false;

	this.keyboard = {
		'ctrl'	: { 'keyCode' :  17, 'keyState' : this.KEY_UP },
		'space'	: { 'keyCode' :  32, 'keyState' : this.KEY_UP },
		'left'	: { 'keyCode' :  37, 'keyState' : this.KEY_UP },
		'up'	: { 'keyCode' :  38, 'keyState' : this.KEY_UP },
		'right'	: { 'keyCode' :  39, 'keyState' : this.KEY_UP },
		'down'	: { 'keyCode' :  40, 'keyState' : this.KEY_UP },
		'c'	: { 'keyCode' :  67, 'keyState' : this.KEY_UP },
		'v'	: { 'keyCode' :  86, 'keyState' : this.KEY_UP },
		'p'	: { 'keyCode' :  80, 'keyState' : this.KEY_UP },
		'x'	: { 'keyCode' :  88, 'keyState' : this.KEY_UP },
		'z'	: { 'keyCode' :  90, 'keyState' : this.KEY_UP },
		'F5'	: { 'keyCode' : 116, 'keyState' : this.KEY_UP },
	}
}


GameEngine.prototype.KeyHandle = function( e, state ) {
	e.preventDefault();
	for ( var keyName in this.keyboard ) {
		if ( e.keyCode == this.keyboard[keyName].keyCode ) {
			this.keyboard[keyName].keyState = state;
			//console.log( "ge.keyboard." + keyName + ' = ' + state );
		}
	}
}


GameEngine.prototype.Start = function() {
	//console.log( player );
	this.lastAnimationTimestamp = Date.now();
	var that = this;
	(
		function TheLoop( timestamp ) {
			if ( timestamp == undefined ) var timestamp = Date.now();
			that.Loop( timestamp );
			requestAnimationFrame( TheLoop );
		}
	)();
}


GameEngine.prototype.CheckPause = function() {
	if ( this.keyboard['p'].keyState == this.KEY_DOWN && this.pauseBlock == false ) {
		this.pauseBlock = true;
		this.pauseOn = !this.pauseOn;
	}

	if ( this.keyboard['p'].keyState == this.KEY_UP ) {
		this.pauseBlock = false;
	}
}


GameEngine.prototype.Loop = function( timestamp ) {
	// timeDelta accumulates when tab not focused so set the maximum
	// also can be a stangely large negative value at the beginning
	this.timeDelta = timestamp - this.lastAnimationTimestamp;
	if ( this.timeDelta < 0 || this.timeDelta > this.timeDeltaMax ) {
		this.timeDelta = this.timeDeltaMax;
	}

	//REFRESH?
	if ( this.keyboard['F5'].keyState == this.KEY_DOWN ) location.reload( true );

	//UPDATE
	this.CheckPause();
	if ( this.pauseOn == false ) {
		pulses.Update();
		squares.Update();
		bonuses.Update();
		fading.Update();
		player.Update();
		alerts.Update();

		//DRAW
		this.canv.width = this.CANV_W; // reset canvas
		for ( var f in fading.sprites ) fading.sprites[f].Draw();
		for ( var p in pulses.sprites ) pulses.sprites[p].Draw();
		for ( var s in squares.sprites ) squares.sprites[s].Draw();
		for ( var b in bonuses.sprites ) bonuses.sprites[b].Draw();
		for ( var a in alerts.texts ) alerts.texts[a].Draw();
		player.sprite.Draw();
		player.domain.Draw();

		this.ctx.fillStyle = 'rgb(0,232,0)';
		this.ctx.font = '24px sans-serif';
		this.ctx.fillText( "\u2665 " + player.health, 16, 32 );
		this.ctx.fillText( "\u2605 " + player.points, 128, 32 );
	} else if ( this.gameOver == false ) {
		this.ctx.fillStyle = 'rgb(232,0,0)';
		this.ctx.font = '64px sans-serif';
		this.ctx.fillText( "PAUSE", this.CANV_W/2 - 2*48, this.CANV_H/2 );
	}

	//GAMEOVER?
	if ( this.gameOver == true ) {
		this.pauseOn = true;
		this.ctx.fillStyle = 'rgb(232,0,0)';
		this.ctx.font = '64px sans-serif';
		this.ctx.fillText( 'GAME OVER', this.CANV_W/2-4*48, this.CANV_H/2 - 32 );
		this.ctx.fillText( '\u2605 ' + player.points, this.CANV_W/2-4*48, this.CANV_H/2 + 64 + 32 );
	}

	this.lastAnimationTimestamp = timestamp;
}
