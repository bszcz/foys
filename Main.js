// Copyright (c) 2012-2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

function Main() {
	window.requestAnimationFrame = window.requestAnimationFrame
		|| window.webkitRequestAnimationFrame
		|| window.mozRequestAnimationFrame
		|| window.oRequestAnimationFrame
		|| window.msRequestAnimationFrame;

	ge = new GameEngine();
	ge.canv = document.getElementById( 'canvas' );
	ge.canv.style.border = 'black solid 1px';
	ge.canv.width = ge.CANV_W;
	ge.canv.height = ge.CANV_H;
	ge.canv.tabIndex = 1;
	ge.canv.focus();
	ge.canv.addEventListener( 'keydown', function( e ) { ge.KeyHandle( e, ge.KEY_DOWN ); }, false );
	ge.canv.addEventListener( 'keyup'  , function( e ) { ge.KeyHandle( e, ge.KEY_UP   ); }, false );
	ge.ctx = ge.canv.getContext( '2d' );

	player = new Player();
	fading = new Fading();		// alreay destroyed
	alerts = new Alerts();		// notification texts
	pulses = new Pulses();		// laser pulses
	squares = new Squares();	// to be destroyed
	bonuses = new Bonuses();	// power ups

	ge.Start();
}
