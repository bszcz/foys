// Copyright (c) 2012-2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

function Bonuses() {
	this.BONUS_W = 32;
	this.BONUS_H = 32;
	this.BONUS_DX = 0.1;
	this.BONUS_DY = 0.1;
	this.BONUS_HEALTH_PROB = 0.0005;
	this.BONUS_SLOMO_PROB = 0.0005;
	this.BONUS_RISE_PROB = 0.0005;
	this.BONUS_THRU_PROB = 0.0005;
	this.BONUS_BIOH_PROB = 0.0004;
	this.BONUS_NUKE_PROB = 0.0002;
	this.sprites = new Array();
	this.slomoBlock = false;
	this.goesThroughBlock = false;
	this.goesThroughCounter = 0;
}


Bonuses.prototype.Update = function() {
	var b = this.sprites.length;
	while ( b-- ) {
		this.sprites[b].Move();

		if ( this.sprites[b].y > ge.CANV_H ) {
			this.sprites.splice( b, 1 );
		} else {
			if ( this.sprites[b].Intersects( player.sprite ) ) {
				this.PowerUp( b );
			}
		}
	}
	//console.log( "bonuses.sprites.length = " + this.sprites.length );

	if ( Math.random() < this.BONUS_HEALTH_PROB ) {
		this.sprites.push( new Rectangle( "\u2665",
			0, 192, 0,
			this.BONUS_W, this.BONUS_H,
			Math.random()*( ge.CANV_W-this.BONUS_W ), -this.BONUS_H,
			this.BONUS_DX*( 0.5 - Math.random() ), this.BONUS_DY*( 1 + Math.random() ) ) );
	}

	if ( Math.random() < this.BONUS_SLOMO_PROB ) {
		this.sprites.push( new Rectangle( "\u231b",
			0, 192, 0,
			this.BONUS_W, this.BONUS_H,
			Math.random()*( ge.CANV_W-this.BONUS_W ), -this.BONUS_H,
			this.BONUS_DX*( 0.5 - Math.random() ), this.BONUS_DY*( 1 + Math.random() ) ) );
	}

	if ( Math.random() < this.BONUS_RISE_PROB ) {
		this.sprites.push( new Rectangle( "\u21a5",
			0, 192, 0,
			this.BONUS_W, this.BONUS_H,
			Math.random()*( ge.CANV_W-this.BONUS_W ), -this.BONUS_H,
			this.BONUS_DX*( 0.5 - Math.random() ), this.BONUS_DY*( 1 + Math.random() ) ) );
	}

	if ( Math.random() < this.BONUS_THRU_PROB ) {
		this.sprites.push( new Rectangle( "\u2300",
			0, 192, 0,
			this.BONUS_W, this.BONUS_H,
			Math.random()*( ge.CANV_W-this.BONUS_W ), -this.BONUS_H,
			this.BONUS_DX*( 0.5 - Math.random() ), this.BONUS_DY*( 1 + Math.random() ) ) );
	}

	if ( Math.random() < this.BONUS_BIOH_PROB ) {
		this.sprites.push( new Rectangle( "\u2623",
			0, 192, 0,
			this.BONUS_W, this.BONUS_H,
			Math.random()*( ge.CANV_W-this.BONUS_W ), -this.BONUS_H,
			this.BONUS_DX*( 0.5 - Math.random() ), this.BONUS_DY*( 1 + Math.random() ) ) );
	}

	if ( Math.random() < this.BONUS_NUKE_PROB ) {
		this.sprites.push( new Rectangle( "\u2622",
			0, 192, 0,
			this.BONUS_W, this.BONUS_H,
			Math.random()*( ge.CANV_W-this.BONUS_W ), -this.BONUS_H,
			this.BONUS_DX*( 0.5 - Math.random() ), this.BONUS_DY*( 1 + Math.random() ) ) );
	}

	if ( ge.timeDelay > 1 ) {
		ge.timeDelayCounter += ge.timeDelta;
	}
	if ( ge.timeDelayCounter > 8000 && this.slomoBlock == false ) {
		alerts.texts.push( new AlertText( "\u231b",
			255, 128, 128,
			player.sprite.x, player.sprite.y - 16 ) );
		this.slomoBlock = true;
	}
	if ( ge.timeDelayCounter > 10000 ) {
		ge.timeDelay = 1;
		ge.timeDelayCounter = 0;
		this.slomoBlock = false;
	}


	if ( pulses.goesThrough == true ) {
		this.goesThroughCounter += ge.timeDelta;
	}
	if ( this.goesThroughCounter > 28000 && this.goesThroughBlock == false ) {
		alerts.texts.push( new AlertText( "\u2300",
			255, 128, 128,
			player.sprite.x, player.sprite.y - 16 ) );
		this.goesThroughBlock = true;
	}
	if ( this.goesThroughCounter > 30000 ) {
		pulses.goesThrough = false;
		this.goesThroughCounter = 0;
		this.goesThroughBlock = false;
	}
}


Bonuses.prototype.PowerUp = function( b ) {
	switch ( this.sprites[b].label ) {
		case "\u2665":
			player.AddHealth( 10 );
			break;
		case "\u231b":
			ge.timeDelay = 5;
			ge.timeDelayCounter = 0;
			break;
		case "\u21a5":
			player.domain.y /= 2;
			break;
		case "\u2300":
			pulses.goesThrough = true;
			break;
		case "\u2623":
			for ( var i = 0; i < 2; i++ ) {
				var s = squares.sprites.length;
				while ( s-- ) {
					squares.Shoot( s );
				}
			}
			break;
		case "\u2622":
			for ( var i = 0; i < 3; i++ ) {
				var s = squares.sprites.length;
				while ( s-- ) {
					squares.Shoot( s );
				}
			}
			break;
		// 267E - infinity
	}

	fading.sprites.push( new Rectangle( this.sprites[b].label,
		127, 255, 127,
		this.sprites[b].w, this.sprites[b].h,
		this.sprites[b].x, this.sprites[b].y,
		this.sprites[b].dx*0.5,	this.sprites[b].dy*0.5
	));

	this.sprites.splice( b, 1 );
}
