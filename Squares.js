// Copyright (c) 2012-2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

function Squares() {
	this.SQ_W_MIN = 16;
	this.SQ_W = 64;
	this.SQ_H = 64;
	this.SQ_DX = 0.15;
	this.SQ_DY = 0.15;
	this.SQ_PROB = 0.05; // probability of adding new one

	this.sprites = new Array();
}


Squares.prototype.Shoot = function( s ) {
	if ( this.sprites[s].w > this.SQ_W_MIN ) {
		for ( var f = 0; f < 4; f++ ) {
			this.sprites.push( new Rectangle( "",
				this.sprites[s].r, this.sprites[s].g, this.sprites[s].b + 24 + Math.floor( 8*Math.random() ),
				this.sprites[s].w/2, this.sprites[s].h/2,
				this.sprites[s].x + Math.floor( f/2 )*this.sprites[s].w/2,
				this.sprites[s].y +           ( f%2 )*this.sprites[s].h/2,
				this.sprites[s].dx + 0.5*this.SQ_DX*( 0.5 - Math.random() ),
				this.sprites[s].dy + 0.5*this.SQ_DY*( 0.5 - Math.random() )
			));
		}

	}

	fading.sprites.push( new Rectangle( "",
		this.sprites[s].b, 255, 255, // copy blue into red
		this.sprites[s].w, this.sprites[s].h,
		this.sprites[s].x,
		this.sprites[s].y,
		this.sprites[s].dx*0.2,
		this.sprites[s].dy*0.2
	));

	player.AddPoints( this.sprites[s].w );
	this.sprites.splice( s, 1 );
}


Squares.prototype.Update = function() {
	var s = this.sprites.length;
	while ( s-- ) {
		this.sprites[s].Move( ge.timeDelay );

		if (this.sprites[s].y > ge.CANV_H ) {
			this.sprites.splice( s, 1 );
		} else {
			var p = pulses.sprites.length;
			while ( p-- ) {
				if ( this.sprites[s].Intersects( pulses.sprites[p] ) ) {
					this.Shoot( s );
					if ( pulses.goesThrough == false ) {
						pulses.sprites.splice( p, 1 );
					}
					break;
				}
			}
		}
	}
	//console.log( "squares.sprites.length = " + this.sprites.length );

	if ( Math.random() < this.SQ_PROB / ge.timeDelay ) {
		this.sprites.push( new Rectangle( "",
			0, 0, 64 + Math.floor( 64 * Math.random() ),
			this.SQ_W, this.SQ_H,
			Math.random()*( ge.CANV_W-this.SQ_W ), -this.SQ_H,
			this.SQ_DX*( 0.5 - Math.random() ), this.SQ_DY*( 1 + Math.random() ) ) );
	}
}
