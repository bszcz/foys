// Copyright (c) 2012-2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

function Pulses() {
	this.PULSE_W = 4;
	this.PULSE_H = 4;
	this.PULSE_DX = 0.3;
	this.PULSE_DY = 0.3;

	this.pulseTime = 0;
	this.pulsePeriod = 250;

	this.goesThrough = false;

	this.sprites = new Array();
}


Pulses.prototype.Update = function () {
	var s = this.sprites.length;
	while ( s-- ) {
		this.sprites[s].Move();
		if (this.sprites[s].y < (-1)*this.sprites[s].h ) this.sprites.splice( s, 1 );
	}
	//console.log( "pulses.sprites.length = " + this.sprites.length );

	this.pulseTime += ge.timeDelta;

	if ( this.pulseTime >= this.pulsePeriod ) {
		this.sprites.push( new Rectangle( "", 0, 127, 0,
			this.PULSE_W, this.PULSE_H,
			player.sprite.x + player.sprite.w/2 - this.PULSE_W/2, player.sprite.y - this.PULSE_H,
			this.PULSE_DX * (
				1.73*ge.keyboard['v'].keyState +
				0.58*ge.keyboard['c'].keyState -
				0.58*ge.keyboard['x'].keyState -
				1.73*ge.keyboard['z'].keyState
			), (-1)*this.PULSE_DY ) );
		this.pulseTime -= this.pulsePeriod;
	}
}
