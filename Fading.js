// Copyright (c) 2012-2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

function Fading() {
	this.sprites = new Array();
}


Fading.prototype.Update = function() {
	var f = this.sprites.length;
	while ( f-- ) {
		this.sprites[f].Move();

		this.sprites[f].r += 2;
		this.sprites[f].g += 2;
		this.sprites[f].b += 2;

		if ( this.sprites[f].r > 255 ) this.sprites[f].r = 255;
		if ( this.sprites[f].g > 255 ) this.sprites[f].g = 255;
		if ( this.sprites[f].b > 255 ) this.sprites[f].b = 255;

		if ( this.sprites[f].r+this.sprites[f].g+this.sprites[f].b >= 3*255 ) {
			this.sprites.splice( f, 1 );
		}
	}
	//console.log( "fading.sprites.length = " + this.sprites.length );
}
