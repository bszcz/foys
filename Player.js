// Copyright (c) 2012-2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

function Player() {
	var PLAY_W = 8;
	var PLAY_H = 8;
	var PLAY_DX = 0.25;
	var PLAY_DY = 0.25;

	var DOMAIN_W = ge.CANV_W;
	var DOMAIN_H = 2;
	var DOMAIN_DX = 0;
	var DOMAIN_DY = 0.002;

	this.health = 100;
	this.points = 0;
	this.sprite = new Rectangle( "", 0, 255, 0, PLAY_W, PLAY_H, (ge.CANV_W+PLAY_W)/2, ge.CANV_H-2*PLAY_H, PLAY_DX, PLAY_DY);
	this.domain = new Rectangle( "", 255, 255, 127, DOMAIN_W, DOMAIN_H, 0, 0, DOMAIN_DX, DOMAIN_DY);
}


Player.prototype.Update = function() {
	this.sprite.x += ge.timeDelta * this.sprite.dx * ( ge.keyboard['right'].keyState - ge.keyboard['left'].keyState );
	this.sprite.y += ge.timeDelta * this.sprite.dy * ( ge.keyboard['down' ].keyState - ge.keyboard['up'  ].keyState );
	//console.log( 'MovePlayer' );
	if ( this.sprite.y < this.domain.y+this.domain.h ) this.sprite.y = this.domain.y+this.domain.h;
	if ( this.sprite.y > ge.CANV_H-this.sprite.h ) this.sprite.y = ge.CANV_H-this.sprite.h;

	if ( this.sprite.x < 0 ) this.sprite.x = 0;
	if ( this.sprite.x > ge.CANV_W-this.sprite.w ) this.sprite.x = ge.CANV_W-this.sprite.w;

	if ( this.domain.y < ge.CANV_H*0.9 ) {
		this.domain.y += ge.timeDelta * this.domain.dy;
	} else {
		this.domain.y = ge.CANV_H*0.9;
	}

	var s = squares.sprites.length;
	while ( s-- ) {
		if ( this.sprite.Intersects( squares.sprites[s] ) ) {
			this.health -= squares.sprites[s].w/4;
			alerts.texts.push( new AlertText( "\u2665", 255, 128, 128,
				this.sprite.x + 16*( 0.5 - Math.random() ), this.sprite.y ) );
			if ( this.health <= 0 ) {
				this.health = 0;
				ge.gameOver = true;
			}
			squares.Shoot( s );
		}
	}
}


Player.prototype.AddPoints = function( pts ) {
	this.points += pts;
	if ( Math.floor( player.points/1000 ) < Math.floor( ( player.points + pts )/1000 ) ) {
		alerts.texts.push( new AlertText( "\u2605 " + 1000 * Math.floor( ( player.points + pts )/1000 ),
			128, 255, 128,
			this.sprite.x, this.sprite.y - 16 ) );
	}
}


Player.prototype.AddHealth = function( pts ) {
	this.health += pts;
	if ( this.health > 100 ) this.health = 100;
	alerts.texts.push( new AlertText( "\u2665",
		128, 255, 128, this.sprite.x, this.sprite.y - 16 ) );
}
