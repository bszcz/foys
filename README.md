F.O.Y.S.
========

First attempt at making an HTML5 Canvas game with JavaScript. Play online at [http://bszcz.org/FOYS](http://bszcz.org/FOYS)


### Keys

arrows - move player  
z,x,c,v - tilt fire  
p - pause game  
F5 - restart game  


### COPYRIGHT / LICENSE

The MIT License (MIT).


